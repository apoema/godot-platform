tool
extends Button

export(String, FILE) var next_scene_path = ""

func _on_button_up():
	get_tree().change_scene(next_scene_path)

func _get_configuration_warning() -> String:
	if not next_scene_path:
		return "Change Scene Button has no Next Scene Path"
	else:
		return ""
