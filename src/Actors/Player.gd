extends Actor

export var stomp_impulse = 1000.0

func _physics_process(delta: float) -> void:
	var is_jump_interrupted: = Input.is_action_just_released("jump") and _velocity.y < 0.0
	var direction: = get_direction()
	_velocity = calculate_move_velocity(_velocity, direction, is_jump_interrupted, delta)
	_velocity = move_and_slide(_velocity, FLOOR_NORMAL)

func get_direction() -> Vector2:
	var horizontal: = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	var vertical: = 0.0
	if Input.is_action_just_pressed("jump") and is_on_floor():
		vertical = -1.0
	else:
		vertical = 1.0
	return Vector2(horizontal, vertical)

func _on_EnemyDetector_area_entered(_area: Area2D) -> void:
	_velocity = calculate_stomp_velocity(_velocity, stomp_impulse)
	print(_velocity)

func _on_EnemyDetector_body_entered(_body):
	die()

func calculate_move_velocity(
			linear_velocity: Vector2,
			direction: Vector2,
			is_jump_interrupted: bool,
			delta: float) -> Vector2:
	# Velocity Assignment
	var out: = linear_velocity
	var current_friction: = floor_friction if is_on_floor() else air_friction
	out = friction(out, current_friction) # Add Friction
	out = accelerate_x(out, input_acceleration.x * direction.x) # Add Input aceleration
	out = accelerate_y(out, gravity * delta) # Add Gravity aceleration
	# Jumping
	if direction.y == -1.0:
		out.y = jump_impulse * direction.y
	if is_jump_interrupted:
		out.y = 0.0
	# Sprite direction
	if Input.is_action_just_released("move_left") or Input.is_action_just_released("move_right"):
		$PlayerAnimatedSprite.play("idle")
	if Input.is_action_just_pressed("move_right"):
		$PlayerAnimatedSprite.scale.x = 1
		$PlayerAnimatedSprite.play("walking")
	elif Input.is_action_just_pressed("move_left"):
		$PlayerAnimatedSprite.scale.x = -1
		$PlayerAnimatedSprite.play("walking")
	
	return out

func calculate_stomp_velocity(linear_velocity: Vector2, impulse: float) -> Vector2:
	var out: = linear_velocity
	out.y = -impulse
	return out

func die() -> void:
	PlayerData.deaths +=1
	queue_free()

func accelerate(current_velocity: Vector2, arg_x, arg_y, max_x, max_y) -> Vector2:
	var out: = current_velocity + Vector2(arg_x, arg_y)
	out.x = abs_upper_bound(out.x, max_x)
	out.y = abs_upper_bound(out.y, max_y)
	return out

func accelerate_x(current_velocity: Vector2, value = 0.0) -> Vector2:
	return accelerate(current_velocity, value, 0.0, speed_limit.x, speed_limit.y)

func accelerate_y(current_velocity: Vector2, value = 0.0) -> Vector2:
	return accelerate(current_velocity, 0.0, value, speed_limit.x, speed_limit.y)

func friction(current_velocity: Vector2, friction) -> Vector2:
	var out: = current_velocity
	if out.x > 0:
		out.x = max(accelerate_x(current_velocity, -friction).x, 0.0)
	elif out.x < 0:
		out.x = min(accelerate_x(current_velocity, friction).x, 0.0)
	return out

func abs_upper_bound(x, b):
	if x > 0:
		return min(x, b)
	elif x < 0:
		return max(x, -b)
	else:
		return x
