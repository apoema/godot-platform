extends KinematicBody2D
class_name Actor

export var speed_limit: = Vector2(300.0, 1000.0)
export var jump_impulse: = 1000.0
export var gravity: = 3000.0
export var floor_friction: = 30.0
export var air_friction:   = 10.0
export var input_acceleration: = Vector2(50.0, 50.0)

var FLOOR_NORMAL = Vector2.UP
var _velocity: = Vector2.ZERO
var enemy_speed = speed_limit * 0.75
