extends Area2D

export var next_scene: PackedScene
onready var anim_player: AnimationPlayer = get_node("AnimationPlayer")

func _get_configuration_warning() -> String:
	if not next_scene:
		return "The Next scene property can't be empty"
	else:
		return ""

func _on_body_entered(body:Node):
	teleport()

func teleport() -> void:
	anim_player.play("fade_in")
	yield(anim_player, "animation_finished")
	get_tree().change_scene_to(next_scene)
