extends Area2D

onready var anim_playr: AnimationPlayer = get_node("AnimationPlayer")
export var score: = 100

func _on_body_entered(_body:Node):
	picked()

func picked() -> void:
	PlayerData.score += score
	anim_playr.play("fade_out")
